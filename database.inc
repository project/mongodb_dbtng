<?php

/**
 * @file
 * Database interface code for MongoDB, falling back to MySQL database servers.
 */

include_once DRUPAL_ROOT . '/includes/database/mysql/database.inc';
include_once DRUPAL_ROOT . '/sites/all/modules/mongodb/mongodb.module';
include_once DRUPAL_ROOT . '/sites/all/modules/mongodb/mongodb_field_storage/mongodb_field_storage.module';


/**
 * @ingroup database
 * @{
 */

class DatabaseConnection_mongo extends DatabaseConnection_mysql {
  // These are tables that have a different name due to field storage.
  static public $table_map = array(
    'node' => 'fields_current.node',
  );
  static public $join_data = array(
    'field_config_instance' => array(
      'field_here' => 'field_id',
      'table' => 'field_config',
      'field_there' => 'id',
    ),
    'users_roles' => array(
      'field_here' => 'rid',
      'table' => 'role',
      'field_there' => 'rid',
    ),
    'node_revision' => array(
      'field_here' => 'vid',
      'table' => 'fields_current.node',
      'field_there' => 'vid',
    ),
    'menu_links' => array(
      'field_here' => 'router_path',
      'table' => 'menu_router',
      'field_there' => 'path',
    ),
    'role_permission' => array(
      'field_here' => 'rid',
      'table' => 'role',
      'field_there' => 'rid',
    ),
  );

  function __construct(array $connection_options = array()) {
    $this->connectionOptions = $connection_options;
    $this->connected = FALSE;
    $this->transactionSupport = FALSE;
    $this->setPrefix(isset($this->connectionOptions['prefix']) ? $this->connectionOptions['prefix'] : '');
  }
  function driver() {
    return 'mongo';
  }
  function query($query, array $args = array(), $options = array()) {
    return $this->mongoQuery($query, $args, $options);
  }

  function mongoQuery($query, $args, $options, $skip = NULL, $limit = NULL) {
    if (!preg_match('/^\s*SELECT/', $query)) {
      return TRUE;
    }
    $extra = array();
    $fields = array();
    // Path whitelist is maintained by other means. User specified date formats
    // are simply unsupported.
    if (in_array($query, array(
      "SELECT SUBSTRING_INDEX(source, '/', 1) AS path FROM {url_alias} GROUP BY path",
      'SELECT df.dfid, df.format, df.type, df.locked, dfl.language FROM {date_formats} df LEFT JOIN {date_format_type} dft ON df.type = dft.type LEFT JOIN {date_format_locale} dfl ON df.format = dfl.format AND df.type = dfl.type ORDER BY df.type, df.format',
      ))) {
      return array();
    }
    // Parse the query.
    $parts = preg_split('/\s+|,|AND|BY|\(|\)|SELECT|LEFT|INNER/', $query, -1, PREG_SPLIT_NO_EMPTY);
    $table = '';
    $state = 0;
    $find = array();
    $order = array();
    $count = FALSE;
    foreach ($parts as $part) {
      switch ($part) {
        case 'FROM':
          $state = 10;
          break;
        case 'JOIN':
          $state = 15;
          break;
        case 'WHERE':
          $state = 20;
          break;
        case 'ORDER':
          $state = 30;
          break;
        case 'GROUP' :
          return new MongoPrefetch();
        case 'OR':
          break 2;
        case 'UNION': case 'HAVING':
          throw new PDOException("$part not implemented");
          break;
        default:
          switch ($state) {
            case 0:
              if ($part == 'AS') {
                $state++;
              }
              elseif (substr($part, 0, 5) == 'COUNT') {
                $count = TRUE;
              }
              else {
                $part = explode('.', $part);
                $field = array_pop($part);
                $alias = $part ? $part[0] : '';
                if ($field == '*') {
                  $fields[$alias] = array();
                }
                elseif ($field != '1') {
                  // Menu links break if I collect $fields[$part] = 1 here as
                  // mongo likes. So we don't that just collect fields for the
                  // sake of fetches.
                  $fields[$alias][$field] = TRUE;
                }
              }
              break;
            case 1:
              // alias skipping
              $state--;
              break;
            case 10:
              $part = substr($part, 1, -1);
              $table = isset(DatabaseConnection_mongo::$table_map[$part]) ? DatabaseConnection_mongo::$table_map[$part] : $part;
              $state++;
              break;
            case 11:
              $aliases[$part] = $table;
              break;
            case 15:
              $part = substr($part, 1, -1);
              $new_table = isset(DatabaseConnection_mongo::$table_map[$part]) ? DatabaseConnection_mongo::$table_map[$part] : $part;
              if ($new_table != $table) {
                $join_data = DatabaseConnection_mongo::$join_data;
                if (isset($join_data[$new_table]) && $join_data[$new_table]['table'] == $table) {
                  $table = $new_table;
                }
                elseif (!isset($join_data[$table]) || $join_data[$table]['table'] != $new_table) {
#                  echo "!$table JOIN $new_table not implemented<br>";
                  return new MongoPrefetch();
                }
              }
              $state++;
              break;
            case 16:
              $aliases[$part] = $new_table;
              break;
            case 20:
              // Handle condition.
              $part = explode('.', $part);
              $field = array_pop($part);
              $condition_table = $table;
              if ($part) {
                $condition_table = isset($aliases[$part[0]]) ? $aliases[$part[0]] : $part[0];
              }
              $state++;
              break;
            case 21:
              $op = $part;
              $prefix = '';
              $state++;
              break;
            case 22:
              $c = $part[0];
              if (is_numeric($part)) {
                $part = (int) $part;
              }
              elseif ($c == ':') {
                $part = isset($args[$part]) ? $part : substr($part, 1);
                $part = $args[$part];
              }
              // Start or inside a qouted string?
              elseif ($prefix || $c == "'") {
                // If there is already a prefix, don't cut the first character.
                // Otherwise, it's a quote and need to be cut.
                $string_start = $prefix ? 0 : 1;
                // If at the end of the quoted string.
                if (substr($part, -1) == "'") {
                  $part = $prefix . substr($part, $string_start, -1);
                }
                else {
                  // Start or continue a multipart quoted string.
                  $prefix .= substr($part, $prefix ? 0 : 1) .' ';
                  continue;
                }
              }
              $find[$condition_table][$field] = _mongodb_field_storage_query_value($part, $op);
              $state = 20;
              break;
            case 30:
              if ($part == 'ASC' || $part == 'DESC') {
                $order[$field] = $part == 'ASC' ? 1 : -1;
              }
              else {
                $part = explode('.', $part);
                $field = array_pop($part);
                $order[$field] = 1;
              }
              break;
          }
      }
    }
    if ($table) {
      $join_fields = array();
      $collection = mongodb_collection($this, $table);
      if (isset(DatabaseConnection_Mongo::$join_data[$table])) {
        $join_data = DatabaseConnection_Mongo::$join_data[$table];
        $join_fields[$join_data['field_here']] = TRUE;
        foreach ($find as $current_table => $current_find) {
          if ($current_table != $table) {
            $alien_result = mongodb_collection($this, $current_table)->find($current_find);
            foreach ($alien_result as $row) {
              $find[$table][$join_data['field_here']]['$in'][] = $row['field_there'];
            }
          }
        }
      }
      foreach ($fields as $alias => $actual_fields) {
        if (!$alias || (isset($aliases[$alias]) && $aliases[$alias] == $table)) {
          $fields = $actual_fields;
          break;
        }
      }
      if ($fields) {
        $fields += $join_fields;
      }
      $cursor = $collection->find($find ? $find[$table] : array(), $fields);
      if ($order) {
        $cursor->sort($order);
      }
      if ($skip) {
        $cursor->skip($skip);
      }
      if ($limit) {
        $cursor->limit($limit);
      }
      if ($count) {
        $cursor = $cursor->count();
      }
      return new MongoPrefetch($this, $table, $cursor, array_keys($fields), isset($options['fetch']) ? $options['fetch'] : PDO::FETCH_OBJ);
    }
    echo 'Unsupported query: '. preg_replace('/\s+/', ' ', $query) ."\n";
  }
  function queryRange($query, $from, $count, array $args = array(), array $options = array()) {
    return $this->mongoQuery($query, $args, $options, $from, $count);
  }
  function nextId($existing_id = 0) {
    return mongodb_next_id('db', $existing_id);
  }
}

class MongoPrefetch implements Iterator {
  protected $array_result = array();
  protected $object_result = array();
  protected $keyed_result = NULL;
  protected $column_result = array();
  function __construct($connection = NULL, $table = '', $cursor = array(), $fields = array(), $fetch = PDO::FETCH_OBJ) {
    if ($connection) {
      if (is_scalar($cursor)) {
        $this->count_result = $cursor;
        return;
      }
      $join_data = FALSE;
      if (isset(DatabaseConnection_mongo::$join_data[$table])) {
        $join_data = DatabaseConnection_mongo::$join_data[$table];
        $collection = mongodb_collection($connection, $join_data['table']);
      }
      foreach ($cursor as $row) {
        if ($join_data && isset($row[$join_data['field_here']])) {
          $supplement = $collection->findOne(array($join_data['field_there'] => $row[$join_data['field_here']]));
          // Protect from broken records. Also, LEFT JOIN.
          if (is_array($supplement)) {
            $row += $supplement;
          }
        }
        unset($row['_id']);
        $key = current($row);
        if (isset($extra[$key])) {
          $row += $extra;
        }
        $this->array_result[] = $row;
        $this->object_result[] = (object) $row;
        if (isset($fields[1]) && isset($row[$fields[0]]) && isset($row[$fields[1]])) {
          $this->keyed_result[$row[$fields[0]]] = $row[$fields[1]];
        }
        $this->column_result[] = current($row);
      }
    }
    $this->fetch = $fetch;
    $this->fields = $fields;
    $this->result = $fetch == PDO::FETCH_ASSOC ? $this->array_result : $this->object_result;
  }

  function fetchAllAssoc($key = NULL, $fetch = PDO::FETCH_OBJ) {
    $result = array();
    foreach ($this->array_result as $row) {
      if (!$key) {
        list($key) = each($row);
      }
      $result[$row[$key]] = $fetch == PDO::FETCH_ASSOC ? $row: (object) $row;
    }
    return $result;
  }
  function fetchAllKeyed($key_index = 0, $value_index = 1) {
    $result = array();
    if (isset($this->keyed_result) && !$key_index && $value_index == 1) {
      return $this->keyed_result;
    }
    foreach ($this->array_result as $row) {
      $result[$row[$this->fields[$key_index]]] = $row[$this->fields[$value_index]];
    }
    return $result;
  }
  function fetchField() {
    // Counts already return have a scalar, just return it.
    if (isset($this->count_result)) {
      return $this->count_result;
    }
    // Otherwise, iterate.
    foreach ($this->array_result as $row) {
      list($key, $value) = each($row);
      return $value;
    }
  }
  function fetchCol($col = 0) {
    return $this->column_result;
  }
  function fetchObject() {
    list($key, $return) = each($this->object_result);
    return $return;
  }
  function fetch() {
    if ($this->fetch == PDO::FETCH_ASSOC) {
      return $this->fetchAssoc();
    }
    else {
      return $this->fetchObject();
    }
  }
  function fetchAssoc() {
    list($key, $return) = each($this->array_result);
    return $return ? $return : array();
  }
  function fetchAll() {
    return $this->object_result;
  }
  function current() {
    return current($this->result);
  }
  function key() {
    return key($this->result);
  }
  function next() {
    next($this->result);
  }
  function rewind() {
    reset($this->result);
  }
  function valid() {
    return current($this->result) !== FALSE;
  }
}

/**
 * @} End of "ingroup database".
 */
