<?php

/**
 * @file
 * Database schema code for MySQL database servers.
 */

include_once DRUPAL_ROOT . '/includes/database/mysql/schema.inc';

/**
 * @ingroup schemaapi
 * @{
 */

class DatabaseSchema_mongo extends DatabaseSchema_mysql {
  function createTable($name, $table) {
    $schema = $table['fields'];
    $schema['_id'] = $name;
    mongodb_collection($this->connection, 'drupal.schema')->insert($schema);
    // TODO: add indexes;
  }
  function tableExists($name) {
    return FALSE;
  }
}

/**
 * @} End of "ingroup schemaapi".
 */
