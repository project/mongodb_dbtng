<?php

/**
 * @ingroup database
 * @{
 */

/**
 * @file
 * Query code for MongoDB, falling back to MySQL embedded database engine.
 */

include_once DRUPAL_ROOT . '/includes/database/mysql/query.inc';

function mongodb_query_insert_path($connection, $insert_fields, $insert_values) {
  $index = array_search('source', $insert_fields);
  $whitelist = variable_get('path_alias_whitelist', array());
  $part = strtok($insert_values[$index], '/');
  if (!isset($whitelist[$part])) {
    $whitelist[$part] = TRUE;
    variable_set('path_alias_whitelist', $parts);
  }
}

function mongodb_query_merge_variable($connection, $key_fields, $insert_fields) {
  // Do not write an empty path alias whitelist.
  return ($key_fields['name'] == 'path_alias_whitelist' && $insert_fields['value'] == "a:0:{}");
}

function _mongodb_query_translate_conditions($conditions) {
  $find = array();
  $conjunction = $conditions['#conjunction'];
  unset($conditions['#conjunction']);
  foreach ($conditions as $condition) {
    $find[$condition['field']] = _mongodb_field_storage_query_value($condition['value'], $condition['operator']);
  }
  return $find;
}

class InsertQuery_mongo extends InsertQuery_mysql {
  function execute() {
    $function = "mongodb_query_insert_$this->table";
    if (function_exists($function) && ($result = $function($this->connection, $this->insertFields, $this->insertValues))) {
      return;
    }
    $schema = mongodb_collection($this->connection, 'drupal.schema')->findOne(array('_id' => $this->table));
    unset($schema['_id']);
    $table_map = DatabaseConnection_mongo::$table_map;
    $table = isset($table_map[$this->table]) ? $table_map[$this->table] : $this->table;
    $collection = mongodb_collection($this->connection, $table);
    $defaults = array();
    $default_map = array(
      'varchar' => '',
      'int' => 0,
      'float' => 0,
      'text' => '',
      'blob' => '',
    );
    $return = NULL;
    foreach ($schema as $field => $data) {
      if ($data['type'] == 'serial') {
        $index = array_search($field, $this->insertFields);
        if ($index === FALSE) {
          $index = count($this->insertFields);
          $this->insertFields[] = $field;
        }
        if (!$this->insertValues) {
          $this->insertValues = array(array());
        }
        foreach ($this->insertValues as &$values) {
          if (empty($values[$index])) {
            $return = mongodb_next_id($table);
            $values[$index] = $return;
          }
        }
      }
      if (!isset($data['default']) && isset($default_map[$data['type']])) {
        $data['default'] = $default_map[$data['type']];
      }
      if (isset($data['default']) && array_search($field, $this->insertFields) === FALSE) {
        $this->insertFields[] = $field;
        $defaults[] = $data['default'];
      }
    }
    foreach ($this->insertValues as $insert_values) {
      $save = array_combine($this->insertFields, array_merge($insert_values, $defaults));
      $collection->insert($save);
    }
    $this->insertValues = array();
    return $return;
  }
}

class UpdateQuery_mongo extends UpdateQuery {
  function execute() {
    $find = _mongodb_query_translate_conditions($this->condition->conditions());
    $function = "mongodb_query_update_$this->table";
    if (function_exists($function) && ($result = $function($this->connection, $find, $this->fields))) {
      return;
    }
    $table_map = DatabaseConnection_mongo::$table_map;
    $table = isset($table_map[$this->table]) ? $table_map[$this->table] : $this->table;
    mongodb_collection($this->connection, $table)->update($find, array('$set' => $this->fields), array('multiple' => TRUE));
  }
}

class MergeQuery_mongo extends MergeQuery {
  function execute() {
    $function = "mongodb_query_merge_$this->table";
    if (function_exists($function) && ($result = $function($this->connection, $this->keyFields, $this->insertFields))) {
      return;
    }
    mongodb_collection($this->connection, $this->table)
      ->update($this->keyFields, $this->insertFields + $this->keyFields, array('upsert' => TRUE));
  }
}

class DeleteQuery_mongo extends DeleteQuery {
  function execute() {
    $find = _mongodb_query_translate_conditions($this->condition->conditions());
    mongodb_collection($this->connection, $this->table)->remove($find);
  }
}

class TruncateQuery_mongo extends TruncateQuery {
  function execute() {
    mongodb_collection($this->connection, $this->table)->drop();
  }
}

/**
 * @} End of "ingroup database".
 */
