<?php

class SelectQuery_mongo extends SelectQuery {
  protected $fetch = PDO::FETCH_OBJ;
  function __construct($table, $alias = NULL, DatabaseConnection $connection, $options = array()) {
    if (isset($options['fetch'])) {
      $this->fetch = $options['fetch'];
    }
    parent::__construct($table, $alias, $connection, $options);
  }
  function execute() {
    if ($this->distinct) {
      throw new PDOException('Distinct is not implemented');
    }
    $tables = array();
    $table_map = DatabaseConnection_mongo::$table_map;
    $join_data = DatabaseConnection_mongo::$join_data;
    foreach ($this->tables as $alias => $table) {
      $mapped_table = isset($table_map[$table['table']]) ? $table_map[$table['table']] : $table['table'];
      // Make short work on shortcuts.
      if ($mapped_table == 'shortcut_set') {
        return new MongoPrefetch();
      }
      // Repeated tables are going in to be listed only once in here.
      $tables[$mapped_table] = $mapped_table;
    }
    if (count($tables) != 1) {
      // This needs to be fixed.
      foreach ($tables as $table) {
        // If a table is referencing another table, remove that.
        if (isset($join_data[$table]) && isset($tables[$join_data[$table]['table']])) {
          unset($tables[$join_data[$table]['table']]);
        }
      }
      if (count($tables) != 1) {
        throw new PDOException('JOIN is not implemented');
      }
    }
    // At thiis point, $tables has 1 element.
    foreach ($tables as $table) {
      $collection = mongodb_collection($this->connection, $table);
      foreach ($this->tables as $alias => $stored_table) {
        if ($stored_table['table'] == $table) {
          break 2;
        }
      }
    }
    $count = FALSE;
    if ($this->expressions) {
      if ($this->expressions['expression']['expression'] = 'COUNT(*)') {
        $count = TRUE;
      }
      elseif ($this->expressions['expression']['expression'] = 'MAX(weight)') {
        $max_weight = 0;
        foreach ($collection->find()->sort(array('weight' => -1))->limit(1) as $row) {
          $max_weight = $row['weight'];
        }
        return new MongoPrefetch($this->connection, $table, $max_weight);
      }
      else {
        var_dump($this->expressions);
        throw new PDOException('Expressions are not implemented');
      }
    }

    $find = array();
    $conditions = $this->where->conditions();
    $ids = array();
    $other_find = array();
    unset($conditions['#conjunction']);
    foreach ($conditions as $condition) {
      if (is_string($condition['field'])) {
        $current_find = _mongodb_field_storage_query_value($condition['value'], $condition['operator']);
        $field = explode('.', $condition['field']);
        if (!isset($field[1]) || $field[0] == $alias) {
          $find[array_pop($field)] = $current_find;
        }
        else {
          $current_table = $this->tables[$field[0]]['table'];
          $other_table = isset($table_map[$current_table]) ? $table_map[$current_table] : $current_table;
          $other_find[$field[1]] = $current_find;
        }
      }
      else {
        $or_conditions = $condition['field']->conditions();
        if ($or_conditions['#conjunction'] == 'OR') {
          $find['$or'] = array();
          unset($or_conditions['#conjunction']);
          foreach ($or_conditions as $key => $and_conditions) {
            if (is_object($and_conditions['field'])) {
              $and_conditions = $and_conditions['field']->conditions();
              unset($and_conditions['#conjunction']);
            }
            else {
              $and_conditions = array($and_conditions);
            }
            foreach ($and_conditions as $and_condition) {
              $field = explode('.', $and_condition['field']);
              $find['$or'][$key][array_pop($field)] = _mongodb_field_storage_query_value($and_condition['value'], $and_condition['operator']);
            }
          }
        }
      }
    }
    if ($other_find) {
      $join_data = DatabaseConnection_mongo::$join_data[$table];
      foreach (mongodb_collection($this->connection, $other_table)->find($other_find, array($join_data['field_there'] => TRUE)) as $row) {
        $find[$join_data['field_here']]['$in'][] = $row[$join_data['field_there']];
      }
    }
    $cursor = $collection->find($find);

    if ($this->group) {
      throw new PDOException('GROUP is not implemented');
    }

    if (count($this->having)) {
      throw new PDOException('HAVING is not implemented');
    }

    if ($this->order) {
      foreach ($this->order as $field => $direction) {
        $sort[$field] = $direction == 'ASC' ? 1 : -1;
      }
      $cursor->sort($sort);
    }

    if (!empty($this->range)) {
      $cursor->skip($this->range['start']);
      $cursor->limit($this->range['length']);
    }

    if ($this->union) {
      throw new PDOException('UNION is not implemented');
    }
    return new MongoPrefetch($this->connection, $table, $count ? $cursor->count() : $cursor, array(), $this->fetch);
  }
}
